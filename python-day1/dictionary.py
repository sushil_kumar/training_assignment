########################
print("#################")


# Creating a Dictionary
# with Integer Keys
Dict = {1: 'sk', 2: 'kk', 3: 'pk' , 'key1':'hello'}
print("\nDictionary with the use of Integer Keys: ")
print(Dict)

print(Dict[1])
print(Dict[2])
print(Dict[3])
print(Dict['key1'])

print(Dict.get(3))


print('#deleting from dict')

print(Dict)
del Dict[3]

print(Dict)


# Creating a Dictionary
Dict = {1: 'sk', 2: 'kk', 3: 'pk' , 'key1':'hello'}

# Deleting a key
# using pop() method
pop_ele = Dict.pop(1)
print('\nDictionary after deletion: ' + str(Dict))
print('Value associated to poped key is: ' + str(pop_ele))



########################
# Deleting entire Dictionary
Dict.clear()
print("\nDeleting Entire Dictionary: ")
print(Dict)



print("#####add and update dictionary")
# Creating an empty Dictionary
Dict = {}
print("Empty Dictionary: ")
print(Dict)
 
# Adding elements one at a time
Dict[0] = 'Geeks'
Dict[2] = 'For'
Dict[3] = 1
print("\nDictionary after adding 3 elements: ")
print(Dict)


# Updating existing Key's Value
Dict[2] = 'Welcome'
print("\nUpdated key value: ")
print(Dict)



# Dictionary with three keys
Dictionary1 = {'A': 'Geeks', 'B': 'For', 'C': 'Geeks'}
 
# Printing keys of dictionary
print(Dictionary1.keys())
 
# Creating empty Dictionary
empty_Dict1 = {}
 
# Printing keys of Empty Dictionary
print(empty_Dict1.keys())



print("##########################################")

# Dictionary with two keys
Dictionary1 = {'A': 'Geeks', 'B': 'For'}
 
# Printing keys of dictionary
print("Keys before Dictionary Updation:")
keys = Dictionary1.keys()
print(keys)
 
print(Dictionary1)
# adding an element to the dictionary
Dictionary1.update({'C':'Geeks'})
 
print('\nAfter dictionary is updated:')
print(keys)


print(Dictionary1)


print("##########################################")
print("# search key in Dictionary")


# Dictionary with three items
dic = { 1: 'Welcome', 2: 'To', 'kl': 'Geeks' }


if 1 in dic:
	print('yes present')
else:
	print("sorry not present")

if 12 in dic:
	print('yes present')
else:
	print("sorry not present")


if 'kl' in dic:
	print('yes present')
else:
	print("sorry not present")

if 'kl' in dic.keys():
	print('yes present')
else:
	print("sorry not present")







