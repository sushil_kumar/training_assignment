################################################
print("#################")


#Creating an  Tuple
Tuple1 = (1,2,3)
print("Tuple element: ")
print (Tuple1)

print("#################")

# Creating a Tuple with
# the use of list
list1 = [1, 2, 4, 5, 6]
print("\nTuple using List: ")
tu=tuple(list1)
print(tu)

#Creating a Tuple
#with Mixed Datatype
Tuple1 = (5, 'Welcome', 7, 'Geeks')
print("\nTuple with Mixed Datatypes: ")
print(Tuple1)


st="gud"
Tuple1 = tuple(st)
print("\nFirst element of Tuple: ")
print(Tuple1)


a, b, c= Tuple1
print("\nValues after unpacking: ")
print(a)
print(b)
print(c)


# Concatenation of tuples
Tuple1 = (0, 1, 2, 3)
Tuple2 = ('Geeks', 'For', 'Geeks')

Tuple3 = Tuple1 + Tuple2

# Printing first Tuple
print("Tuple 1: ")
print(Tuple1)

# Printing Second Tuple
print("\nTuple2: ")
print(Tuple2)

# Printing Final Tuple
print("\nTuples after Concatenation: ")
print(Tuple3)



print("############### Slicing of a Tuple")

# Slicing of a Tuple
# with Numbers
Tuple1 = tuple('GEEKSFORGEEKS')

# Removing First element
print("Removal of First Element: ")
print(Tuple1[1:])

# Reversing the Tuple
print("\nTuple after sequence of Element is reversed: ")
print(Tuple1[::-1])

# Printing elements of a Range
print("\nPrinting elements between Range 4-9: ")
print(Tuple1[4:9])





numbers = [1,2,3,4]
  
# start parameter is not provided
Sum = sum(numbers)
print(Sum)
  


#### deleting tuple

del numbers












