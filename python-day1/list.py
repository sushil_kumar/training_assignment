

#########################
########################
print("#################")




# Creating a list
List = [1, 2, 3]
print("\nList with the use of Numbers: ")
print(List)

# Creating a list
List = [1, 2, 3, 'sk', 4, 'is', 6, 'here']
print("\nList with the use of Mixed Values: ")
print(List)



# Creating a Multi-Dimensional list
List = [['i', 'am', 'ok'] , 'sk', [1 , 2 , 3]]
print("\nMulti-Dimensional list: ")


print(List)

print(List[0])
print(List[1])
print(List[2])



########################
print("#################")


List=[10,20]


print(List)

List.append(1)
List.append(2)
List.append(4)

print(List)

########################
print("#################")


# Addition of Element at  specific Position using Insert

List = [1,2,3,4]
print("Initial List: ")
print(List)
  

List.insert(3, 12)

print("\nList after performing Insert Operation: ")
print(List)



########################
 #Addition of multiple elements
# to the List at the end
# (using Extend Method)
List.extend([800, 'learn',45, 'Always'])
print("\nList after performing Extend Operation: ")
print(List)




# Removing elements from List
# using Remove() method
List.remove(800)
List.remove(45)
print("\nList after Removal of two elements: ")
print(List)



#################################################

# Removing element from the
# Set using the pop() method

List = [10,20,30,40,50]
List.pop() #remove last element
print("\nList after popping an element: ")
print(List)

# Removing element at a
# specific location from the
# Set using the pop() method
List.pop(2)  # 
print("\nList after popping a specific element: ")
print(List)






# list slicing

List = ['a','b','c','d','e','f']
print("Initial List: ")
print(List)

# Print elements of a range
# using Slice operation

print("\nSlicing elements in a range : ")
print(List[1:6])

print(List[0:6])

print(List[0:6:2])




print("List Comprehention")
# List Comprehention
y=[1,3,5]
square = []
for x in y:
    square.append(x * x)
    
print(square)   
# by using list comprehention 

square2 = [x * x for x in y]
print(square2)   


square3 = [x ** 2 for x in range(1, 12) if x % 2 == 1]

print(square3)


print("# get index of givn element")
# get index of givn element


list1 = [1, 2, 3, 4, 1, 1, 1, 4, 5]

# Will print the index of '4' in list1
print(list1.index(4))

list2 = ['cat', 'bat', 'mat', 'cat', 'pet']

# Will print the index of 'cat' in list2
print(list2.index('cat'))




print("# Counts the number of times 1 appears in list1")
ist1 = [1, 1, 1, 2, 3, 2, 1]
 

print(list1.count(1))




print("sorting of list uusing sort function")

# List of Integers
numbers = [1, 3, 4, 2]

# Sorting list of Integers
numbers.sort()

print(numbers)


numbers.sort(reverse=True)

print(numbers)

# List of Floating point numbers
decimalnumber = [2.01, 2.00, 3.67, 3.28, 1.68]

# Sorting list of Floating point numbers
decimalnumber.sort()

print(decimalnumber)

# List of strings
words = ["Geeks", "For", "Geeks"]

# Sorting list of strings
words.sort()

print(words)
words.reverse()
print(words)



















































