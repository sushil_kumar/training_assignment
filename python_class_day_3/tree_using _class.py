#### Binary Tree


class node:
	def __init__(self, key):
		self.left = None
		self.right = None
		self.val = key



def print_inorder(root):

	if root:


		print_inorder(root.left)
		print(root.val,end=" "),
		print_inorder(root.right)



def print_postorder(root):

	if root:
		print_postorder(root.left)
		print_postorder(root.right)
		print(root.val,end=" "),



def print_preorder(root):

	if root:
		print(root.val,end=" "),
		print_preorder(root.left)
		print_preorder(root.right)


####################
root = node(1)
root.left = node(2)
root.right = node(3)
root.left.left = node(4)
root.left.right = node(5)
root.right.left = node(6)
root.right.right = node(7)

print("inorder traversal") 
print_inorder(root)
print()

print("preorder traversal")
print_preorder(root)
print()

print("postorder traversal")
print_postorder(root)
print()





















