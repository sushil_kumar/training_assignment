
print("-----no parameter------")
class person:
	
	
	fn = "sushil"
	ln = "kumar"

	# A sample method
	def fun(self):
		print("I'm a", self.fn)
		print("I'm a", self.ln)


p=person()

p.fun()




###
print("------passing two parameter-------------")

class person1:
	
	
	fn = "sushil"
	ln = "kumar"

	# A sample method
	def fun(self,a=None,b=None):
		self.fn=a
		self.ln=b
		print("I'm a", self.fn)
		print("I'm a", self.ln)



p1=person1()

p1.fun("Ak","pk")

print("---------passing one paramerter out of two----------")

p2=person1()

p2.fun("aniket")


print("----------passing zero parameter out of two---------")

p3=person1()

p3.fun()



























