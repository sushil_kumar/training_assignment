print("#################")


print("####### any fun ##########")

# Here all the iterables are True so all
# will return True and the same will be printed
print (all([True, True, True, True]))

# Here the method will short-circuit at the
# first item (False) and will return False.
print (all([False, True, True, False]))

# This statement will return False, as no
# True is found in the iterables
print (all([False, False, False]))


# Since all are false, false is returned
print (any([False, False, False, False]))


# second item (True) and will return True.
print (any([False, True, False, False]))


# first (True) and will return True.
print (any([True, False, False, False]))

print("with numbers")
print (any([0,0,0]))
print (any([0,0,3]))
print (any([0,-2,0]))
print (any([-2,-3,-4]))
print("empty list")
print(any([]))



print("####### all fun ##########")

# will return True and the same will be printed
print (all([True, True, True, True]))

# Here the method will short-circuit at the
# first item (False) and will return False.
print (all([False, True, True, False]))

# This statement will return False, as no
# True is found in the iterables
print (all([False, False, False]))

print("with numbers")
print (all([0,0,0]))
print (all([0,0,3]))
print (all([0,-2,0]))
print (all([-2,-3,-4]))

print("empty list")
print(all([]))



print("exmple with tuple")
print()
tuple=(1,2,3)
print(any(tuple))
print(all(tuple))

tuple2=(1,2,0)
print(any(tuple2))
print(all(tuple2))
print()
print()


print("###### sort and sorted function ")
print()

# Dictionary
x = {'q':1, 'w':2, 'e':3, 'r':4, 't':5, 'y':6}
print(x)
print()

print (sorted(x))


 #List of Integers 
numbers = [1, 3, 4, 2] 

print (sorted(numbers))
print("oroginal list",numbers)
# Sorting list of Integers 
numbers.sort() 
    
print(numbers) 

print()
print(" ### python range function")
# printing a number
for i in range(10):
    print(i, end =" ")
print()
  
# using range for iteration
l = [10, 20, 30, 40]
for i in range(len(l)):
    print(l[i], end =" ")
print()


# printing a number
for i in range(1,10):
    print(i, end =" ")
print()

# printing a number
for i in range(8,10):
    print(i, end =" ")
print()


print(" ### python range function with postive increment")
# printing a number
for i in range(0,10,1):
    print(i, end =" ")
print()


# printing a number
for i in range(0,10,2):
    print(i, end =" ")
print()

# printing a number
for i in range(0,10,3):
    print(i, end =" ")
print()

# printing a number
for i in range(0,10,4):
    print(i, end =" ")
print()


print(" ### python range function with negative increment")
# printing a number
for i in range(10,0,-1):
    print(i, end =" ")
print()


# printing a number
for i in range(10,0,-2):
    print(i, end =" ")
print()



print(" ### python enumrate functionn")
# Python program to illustrate
# enumerate function
l1 = ["eat","sleep","repeat"]
s1 = "abcdf"


print ("Return type:",type(enumerate(l1)))
print ("Return type:",type(enumerate(s1)))

print (list(enumerate(l1)))

# changing start index to 2 from 0
print (list(enumerate(s1,0)))
print (list(enumerate(s1,1)))
print (list(enumerate(s1,-1)))
print (list(enumerate(s1,10)))


print("----------------")


# Python program to illustrate
# enumerate function in loops
l1 = ["eat","sleep","repeat"]

# printing the tuples in object directly
for ele in enumerate(l1):
	print (ele)

print()

# changing index and printing separately
for count,ele in enumerate(l1,10):
	print (count,ele)

#getting desired output from tuple
for count,ele in enumerate(l1):
	print(count)
	print(ele)



print("-------zip function---------")


# initializing lists
name = [ "Manjeet", "Nikhil", "Shambhavi", "Astha" ]
roll_no = [ 4, 1, 3, 2 ]
marks = [ 40, 50, 60, 70 ]

# using zip() to map values
data = zip(name, roll_no, marks)

# converting values to print as set
mapped = list(data)

# printing resultant values
print ("The zipped result is : ")
print (mapped)






print("----------------")


print("-------zip function with differ size---------")


# initializing lists
name = [ "Manjeet", "Nikhil", "Shambhavi", "Astha" ]
roll_no = [ 4, 1, 3 ]
marks = [ 40, 50, 60, 44, 566, 444 ]

# using zip() to map values
data = zip(name, roll_no, marks)

# converting values to print as set
mapped = list(data)

# printing resultant values
print ("The zipped result is : ")
print (mapped)



print()
print("------- # unzipping values--------")


name1 = [ "Manjeet", "Nikhil", "Shambhavi", "Astha" ]
roll1 = [ 4, 1, 3, 2 ]
marks1 = [ 40, 50, 60, 70 ]

# using zip() to map values
data = zip(name1, roll1, marks1)

namz, roll, marksz = zip(*data)
  
print ("The unzipped result: \n",end="")
  
# printing initial lists
print ("The name list is : ",end="")
print (namz)
  
print ("The roll_no list is : ",end="")
print (roll)
  
print ("The marks list is : ",end="")
print (marksz)





print()
print("-------###iterators--------")
print()










