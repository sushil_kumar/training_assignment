################################################


print()
print("-------functions---------------")
print()




def fun1():
	print("this is in function")
	
fun1()

print()
print()

def fun2(x):
	print(x)
	
x="this is pass to the fun2"
fun2(x)



####################################

print("-------functions---------------")

def fun3(x):
	x=x+3
	print("x in side the fun3",x)
	
x=10
print("x befor fun3 call",x)
fun3(x)
print("x befor fun3 call",x)



####################################

print("-------functions---------------")

# Here x is a new reference to same list lst
def myFun(x):
	x[0] = 12


#  lst is modified
# after function call.
lst = [10, 20, 30]

print("befor call",lst)
myFun(lst)
print("after call",lst)




####################
print()
print()
print("# call by reference")


def add_more(list):
	list.append(50)
	print("Inside Function", list)

# Driver's code
mylist = [10,20]

add_more(mylist)
print("Outside Function:", mylist)





####################
print("############function agumrnts ")
print()

def fun4(x,y,z):
	print("x is : ",x)
	print("y is : ",y)
	print("z is : ",z)



fun4(1,2,3)




####################
print()
print()

def fun4(q,r,n):
	print("quantiy is : ",q)
	print("rate is : ",r)
	print("name is : ",n)



fun4(3,4.5,"rice")


####################
print()
print()

def fun5(q,r,n):
	print("quantiy is : ",q)
	print("rate is : ",r)
	print("name is : ",n)



fun5(3,"rice",4.5)

##########################
print()
print("to avoid cinfusion used arument name with value")


def fun6(n,r,q):
	print("quantiy is : ",q)
	print("rate is : ",r)
	print("name is : ",n)



fun6(q=3,n="rice",r=4.5)


print()



print("############args for variable number of arguments")


def myFun(a,b,*argv):
	print(a,"  and  ",b)
	for arg in argv:
		print(arg)


myFun(2,4,'Hello', 'Welcome', 'in', 'myhome')





#
print("#######Argument Dictionary Packing and Unpacking (**)wargs")



def myFun(x,y,**pr):
	for key, value in pr.items():
		print(key," ",value)


# Driver code
myFun(22,33,qunatity=2, rate=4.5, name="bread")


print()
print("#####only argument name######")

def myFun(**pr):
	for var in pr:
		print(var)


# Driver code
myFun(qunatity=2, rate=4.5, name="bread")




##############################################################
print()
print("###default value of parameter######")

# default arguments

def myFun(x=11, y=22):
	print("x: ", x)
	print("y: ", y)


# calling myFun() without argument)
myFun()


##################
print()
# problem with default arguments

def skf(l=[]):
	l.append("kk")
	return l



lst=[12]
print(skf(lst))


print(skf(lst))


###################
print("below is problem")
print()
print()
######################


def show_my_gadgets(gadget, gadget_list=[]):
    gadget_list.append(gadget)
    return (gadget_list)


w=show_my_gadgets("kk")
print(w)

w=show_my_gadgets("pk")
print(w)

w=show_my_gadgets("dk")
print(w)



print()
#############

print("problem is solve dby below method")
def show_my_gadgets(gadget, gadget_list=None):
    if gadget_list is None:
        gadget_list = []
    gadget_list.append(gadget)
    return(gadget_list)


ll=[]

ans=show_my_gadgets("hi")
print(ans)

ans=show_my_gadgets("by")
print(ans)

ans=show_my_gadgets("ok")
print(ans)




























