
print()
print("-------###iterators--------")
print()


mylist = [4, 7, 10, 3]

# get an iterator using iter()

my_iter = iter(mylist)



# Output: 4
print(next(my_iter))


# Output: 7
print(next(my_iter))




# Output: 0
print(next(my_iter))

# Output: 3
print(next(my_iter))





print("-------###iterators using while loop-------")

list=[33,44,55,66,99]
myiter=iter(list)
 
while True:
    try:
 
        # Iterate by calling next
        item = next(myiter)
        print(item)
    except:
 
        # exception will happen when iteration will over
        break



print()
print("-------###Genrators--------")
print()

def simpleGeneratorFun():
    yield 1
    yield 2
    yield 3
  
# Driver code to check above generator function
for value in simpleGeneratorFun(): 
    print(value)




print("# generator object with next() ")
  
# A generator function
def simpleGeneratorFun():
    yield 1
    yield 22
    yield 333
   
# x is a generator object
x = simpleGeneratorFun()
  
# Iterating over the generator object using next
print(next(x)) 
print(next(x))
print(next(x))






print()
print()
print()

# generate squares from 1to 100 using
# yield and therefore generator

# An infinite generator function that prints
# next square number. It starts with 1

def nextSquare():
	i = 1

	# An Infinite loop to generate squares
	while True:
		yield i*i				
		i += 1 # Next execution resumes
				# from this point	

# Driver code to test above generator
# function
for num in nextSquare():
	if num > 25:
		break	
	print(num)

#################
print()
print()
print()

gen=nextSquare()
v=v=next(gen)

while( v<=36 ):
	print(v)
	v=next(gen)


#################
print()
print("generator fun with patrametwr")
print()

def square(n):
	for i in range(n):
		yield i*i


a = square(3)

print("The square of numbers are : ")
print(next(a))
print(next(a))
print(next(a))







































