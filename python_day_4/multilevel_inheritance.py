# multi- level inheritance

class manufacture(object):
	def __init__(self,mfname,mfdate,mfcountry,mfwarrenty):
		self.mfname=mfname
		self.mfdate=mfdate
		self.mfcountry=mfcountry
		self.mfwarrenty=mfwarrenty


class software(manufacture):
	def __init__(self,os,osver,mfname,mfdate,mfcountry,mfwarrenty):
		self.os=os
		self.osver=osver
		
		manufacture.__init__(self,mfname,mfdate,mfcountry,mfwarrenty)

class hardware(software):
	def __init__(self,ram,rom,pro,os,osver,mfname,mfdate,mfcountry,mfwarrenty):
		self.ram=ram
		self.rom=rom
		self.pro=pro
		
		software.__init__(self,os,osver,mfname,mfdate,mfcountry,mfwarrenty)


class device(hardware):
	def __init__(self,name,dtype,ram,rom,pro,os,osver,mfname,mfdate,mfcountry,mfwarrenty):
		self.name=name
		self.dtype=dtype
		
		hardware.__init__(self,ram,rom,pro,os,osver,mfname,mfdate,mfcountry,mfwarrenty)

	def feature(self):
		print("1. device name is                 : ",self.name)
		print("2. device type is is              : ",self.dtype)
		print("3. device ram is                  : ",self.ram)
		print("4. device rom is                  : ",self.rom)
		print("5. device processor is            : ",self.pro)
		print("6. device os is                   : ",self.os)
		print("7. device os version is           : ",self.osver)
		print("8. device manufacture             : ",self.mfname)
		print("9. device manufacturing date is   : ",self.mfdate)
		print("10. device manufacturing country  : ",self.mfcountry)
		print("11. device warrenty is            : ",self.mfwarrenty)






##  ( name , dtype , ram , rom , pro , os , osver , mfname , mfdate , mfcountry , mfwarrenty )

dev=device("sk_mobile","phone","4gb","64gb","intell","android","orio","nokia","june-2021","india","2-year")


dev.feature()

print('\n',dev.__dict__,'\n')


dev.osver="kitkat"

dev.feature()


