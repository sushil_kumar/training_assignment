

class test_singleton(object):
    x = None

    def __new__(self,*args):
        if not test_singleton.x:
            test_singleton.x = object.__new__(self)
        return test_singleton.x

    def __init__(self, first_name="iron", last_name="man"):
        self.first_name = first_name
        self.last_name  = last_name

s1 = test_singleton("rahul", "rahod")
s2 = test_singleton("sorab", "sagar")
s3 = test_singleton()



print(s1)

print(s2)

print(s3)



print("\n-------------------------------\n")

print(s1.last_name," ",s1.first_name)

print(s2.last_name," ",s2.first_name)

print("\nnow changing first and last name ")

s3.first_name="sushil"
s3.last_name="kumar"

print("\n-------------------------------\n")



print(s1.last_name," ",s1.first_name)

print(s2.last_name," ",s2.first_name)
















